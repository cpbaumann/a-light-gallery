<?php

	/*
	*	the picture and metadata are in
	*	a folder beside the project folder!
	* 	master!
	*/


	$config = array();

	###  global settings
	$config['size_th'] = 'thumb';
	$config['size_sm'] = 'small';
	$config['size_la'] = 'large';

	$config['gallery']['version'] = 'a light gallery';
	# the base folder
	$config['gallery']['lgfolder'] = '/a-light-gallery';
	# use rewrite true or false
	$config['gallery']['rewrite'] = true;
	# use absolute, absolute-docroot, relative path 
	$config['gallery']['pathtype'] = 'absolute-docroot';


	# $config['gallery']['host'] depends on DEVELOPMENT_MODE value


	### gallery settings
	$config['lgfolder'][1] = $config['gallery']['lgfolder'];
	$config['folders'][1] = 'the-hotel-simple';
	$config['name'][1] = 'The Hotel by J. H.';
	$config['attr_height_with'][1] = false;
	$config['gallery']['filedatabase'][1] = false;
	$config['theme'][1] = 'new';


	$config['lgfolder'][2] = $config['gallery']['lgfolder'];
	$config['folders'][2] = 'the-hotel-dark';
	$config['name'][2] = 'The Hotel by J. H.';
	$config['attr_height_with'][2] = false;
	$config['theme'][2] = 'dark';


	$config['lgfolder'][3] = $config['gallery']['lgfolder'];
	$config['folders'][3] = 'the-hotel-dark';
	$config['name'][3] = 'The Hotel by J. H.';
	$config['attr_height_with'][3] = false;
	$config['theme'][3] = 'simpledark';


	$config['lgfolder'][4] = $config['gallery']['lgfolder'];
	$config['folders'][4] = 'the-hotel-dark';
	$config['name'][4] = 'The Hotel by J. H.';
	$config['attr_height_with'][4] = false;
	$config['theme'][4] = 'dev';




	if (!defined('DEVELOPMENT_MODE')){
		define('DEVELOPMENT_MODE',true);
	}

	if (DEVELOPMENT_MODE === true){
		ini_set('error_reporting',E_ALL | E_STRICT);
		ini_set('log_errors',1);
		ini_set('display_errors',1);

		if (!defined('DEBUG')){
			define ('DEBUG', true);
		}

		if (!isset($config['gallery']['host'])){
			$config['gallery']['host'] = 'localhost';
		}
	}

	if (DEVELOPMENT_MODE === false){
		ini_set('error_reporting',E_ALL & ~E_DEPRECATED);
		ini_set('log_errors',1);
		ini_set('display_errors',0);

		if (!isset($config['gallery']['host'])){
			$config['gallery']['host'] = 'example.com';
		}
		define ('DEBUG', false);
	}
