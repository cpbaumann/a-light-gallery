<?php

	class ControllerLightGallery{
		
		function __construct($config){
			



			# global filter
			$req = new SecureRequest;


			if( false === $req->filter_keys(
						$allowed_keys = array('p','g','n','type'),
						$type = 'GET')){
				exit('wrong var parameter!');
			}

			if( false === $req->filter_values(
						$var = 'type',
						$values = array('th','sm','la'),
						$type = 'GET')){
				exit('wrong parameter value!');
			}


			if( false === $req->filter_notnumeric_values(
						$value = 'g',
						$type = 'GET')){
				exit('wrong parameter value g!');
			}

			if( false === $req->filter_notnumeric_values(
						$value = 'p',
						$type = 'GET')){
				exit('wrong parameter value p!');
			}

			if( false === $req->filter_notnumeric_values(
						$value = 'n',
						$type = 'GET')){
				exit('wrong parameter value n!');
			}


			if(
				!isset($_GET['g']) or $_GET['g'] == ''
				or !isset($_GET['p']) or $_GET['p'] == ''
				or !isset($_GET['n']) or $_GET['n'] == ''
				or !isset($_GET['type']) or $_GET['type'] == ''
			){

				$gallery = 1;
				$p = 0; 
				$n = 0;
				$type = 'sm'; 

				if(true === $config['gallery']['rewrite']){
					$url = 'http://' 
					 . $config['gallery']['host'] 
					 . $config['gallery']['lgfolder'] 
					 . '/' 
					 . $gallery 
					 . '/'
					 . $p
					 . '/'
					 . $n
					 . '/'  
					 . $type;
				}else{
					$url = 'http://' 
					 . $config['gallery']['host'] 
					 . $config['gallery']['lgfolder']
					 . '/?'
					 . 'g=' . $gallery 
					 . '&p=' . $p 
					 . '&n=' . $n 
					 . '&type=' . $type;
				}

				header ("location: $url");
				

			}else{
				if(isset($config['folders'][$_GET['g']])){
					$gallery = $_GET['g'];
				}else{
					exit('gallery do not exist!');
				}
				$p = $_GET['p'];
				$n = $_GET['n'];
				$type = $_GET['type'];
			}


			$lightgallery = new DataLightGallery;


			$folder = dirname(__FILE__) . '/../../' . $config['folders'][$gallery] . '/';

			if($config['gallery']['filedatabase'] === true){
				
				if (file_exists( $folder . 'conf/conf.dat')){
					$data = $lightgallery->read_conf( $folder . 'conf/conf.dat');
				}else{
					exit('filedatabase do not exist!');
				}	

			}else{
				$data_th = $lightgallery->search_files(
					$folder . $config['size_th'] . '/',
					$config['size_th']
				);

				$data_sm = $lightgallery->search_files(
					$folder . $config['size_sm'] . '/',
					$config['size_sm']
				);

				$data_la = $lightgallery->search_files(
					$folder . $config['size_la'] . '/',
					$config['size_la']
				);

				$data = array_merge($data_th, $data_sm, $data_la);

			}

			$v = new ViewLightGallery($data, $config);


			if(!@file_exists('gallery/themes/' . $config['theme'][$gallery] . '/template/page.php')) {
				echo 'can not include template: gallery/themes/' . $config['theme'][$gallery] . '/template/page.php';
			}else{
				include_once('gallery/themes/' . $config['theme'][$gallery] . '/template/page.php');
			}
		}
	}
	