<?php

class DataLightGallery{

	var $size;
	var $path;
	var $order;

	function search_files($path, $size, $order = SORT_STRING){

		$this->path = $path;
		$this->size = $size;
		$this->order = $order;

		if(!is_dir($this->path)){
			return false;
		}

		if($this->size == ''){
			return false;
		}

		$handle = opendir($this->path);
		while ($array_dir[] = readdir($handle)) {}
		closedir($handle);

		sort($array_dir, $this->order);

		foreach($array_dir as $file){
			if (!preg_match('/^\./', $file) and strlen($file)){
				if (!is_dir($path . '/' . $file)){
					if (preg_match('/' . $this->size . '/', $file)){
						$found_files[] = $file;
					} 
				}
			}
		}

		if (count($found_files) < '1'){
			return false;
		}

		$i = 0;
		foreach($found_files as $file){
			$imagesize = getimagesize($this->path . $file);
			# (1 = GIF, 2 = JPG, 3 = PNG
			if(($imagesize[2] == 1) or ($imagesize[2] == 2) or ($imagesize[2] == 3)){

				#	the rule: number first after @ followed with a-zA-Z and any -_ (but not numbers)
				#	yx[th,la,sm] @ numbers[1-] a-zA-z . yxz[png.gif.jpg]

				$expression = '/([a-zA-Z]{2})@([0-9]{1,})([a-zA-Z\-_]{2,}).([a-zA-Z]{3})/';
				if(preg_match ($expression ,$file, $regs)){
					$number = $regs[2];
					$number = $this->cut_str($number);
					$array_files[$this->size][$i]['filename'] = $file;
					$array_files[$this->size][$i]['found_number'] = $number;
					$i++;
				}
			}
		}

		ksort($array_files[$this->size]);
		usort($array_files[$this->size], array(&$this, 'compare'));

	return $array_files;
	}


	function compare($a, $b){
		$this->a = $a;
		$this->b = $b;
		return strnatcasecmp($this->a['found_number'], $this->b['found_number']);
	}


	/*
	*	coops 0 at the begining of a number
	*/
	function cut_str($str_number){

		$this->str_number =  $str_number;

		if(preg_match('/^0/', $this->str_number)) {
			$this->str_number = substr($this->str_number, 1);
			$this->cut_str($this->str_number);
		}
		return ($this->str_number);
	}


	/*
	*	read the flatfile
	*
	*/
	function read_conf($file_name){

		$handle = fopen ($file_name,"r");
		while (($d = fgetcsv ($handle, 1000, ";")) !== FALSE){
			$data['thumb'][$d['0']]['filename'] = 'thumb@' . $d['1'];
			$data['small'][$d['0']]['filename'] = 'small@' . $d['1'];
			$data['large'][$d['0']]['filename'] = 'large@' . $d['1'];
		}
		fclose ($handle);
		return $data;
	}
}