<?php

class SecureRequest{

	var $var;
	var $values;
	var $allowed_keys;
	var $value;

	function filter_values($var,$values,$type = 'REQUEST'){

		$this->_var = $var;
		$this->_values = $values;
		
		if ('REQUEST' == $type){
			if (isset($_REQUEST[$this->_var]) AND (!empty($_REQUEST[$this->_var]))){
				if (!in_array($_REQUEST[$this->_var], $this->_values)){
					unset($_REQUEST[$this->_var]);
					return false;
				}else{
					return true;
				}
			}else{
				return true;
			}
		}

		if ('POST' == $type){
			if (isset($_POST[$this->_var]) AND (!empty($_POST[$this->_var]))){
				if (!in_array($_POST[$this->_var], $this->_values)){
					unset($_POST[$this->_var]);
					return false;
				}else{
					return true;
				}
			}else{
				return true;
			}
		}

		if ('GET' == $type){
			if (isset($_GET[$this->_var]) AND (!empty($_GET[$this->_var]))){
				if (!in_array($_GET[$this->_var], $this->_values)){
					unset($_GET[$this->_var]);
					return false;
				}else{
					return true;
				}
			}else{
				return true;
			}
		}
		
	}

	function filter_keys($allowed_keys, $type = 'REQUEST'){

		$this->allowed_keys = $allowed_keys;

		if ('REQUEST' == $type){
			foreach($_REQUEST as $key => $value){
				if (is_array($this->allowed_keys)){
					if (!in_array($key, $this->allowed_keys)){
						unset($_REQUEST[$key]);
						return false;
					}
				}else{
					if ($key != $this->allowed_keys){
						unset($_REQUEST[$key]);
						return false;
					}
				}
			}
		}

		if ('POST' == $type){
			foreach($_POST as $key => $value){
				if (is_array($this->allowed_keys)){
					if (!in_array($key, $this->allowed_keys)){
						unset($_POST[$key]);
						return false;
					}
				}else{
					if ($key != $this->allowed_keys){
						unset($_POST[$key]);
						return false;
					}
				}
			}
		}

		if ('GET' == $type){
			foreach($_GET as $key => $value){
				if (is_array($this->allowed_keys)){
					if (!in_array($key, $this->allowed_keys)){
						unset($_GET[$key]); 
						return false;
					}
				}else{
					if ($key != $this->allowed_keys){
						unset($_GET[$key]);
						return false;
					}
				}
			}
		}
		
	}

	function filter_notnumeric_values($value, $type = 'REQUEST'){

		$this->_value = $value;
		
		if ('REQUEST' == $type){
			if (isset($_REQUEST[$this->_value]) AND (!empty($_REQUEST[$this->_value]))){
				if(!preg_match("/^[1-9]{1}[0-9]{0,}$/", $_REQUEST[$this->_value])){
					unset($_REQUEST[$this->_value]);
					return false;
				}else{
					return true;
				}
			}else{
				return true;
			}
		}

		if ('POST' == $type){
			if (isset($_POST[$this->_value]) AND (!empty($_POST[$this->_value]))){
				if(!preg_match("/^[1-9]{1}[0-9]{0,}$/", $_POST[$this->_value])){
					unset($_POST[$this->_value]);
					return false;
				}else{
					return true;
				}
			}else{
				return true;
			}
		}

		if ('GET' == $type){
			if (isset($_GET[$this->_value]) AND (!empty($_GET[$this->_value]))){
				if(!preg_match("/^[1-9]{1}[0-9]{0,}$/", $_GET[$this->_value])){
					unset($_GET[$this->_value]);
					return false;
				}else{
					return true;
				}
			}else{
				return true;
			}
		}
	}
}
?>
