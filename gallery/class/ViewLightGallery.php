<?php

/*
*	with getimagesize!
*
*/

class ViewLightGallery{

	public function __construct($data,$config){
		$this->data = $data;
		$this->config = $config;
	}

	public function thumb3($size,$nu,$n,$p,$linktotype,$gallery,$text){
		
		$html = '';
		if(isset($data[$size][$nu]['filename'])):
			$filename = $this->data[$size][$nu]['filename'];
		else:
			$filename = NULL;
		endif;

		if(is_file($this->config['folders'][$gallery] . '/' . $size . '/'. $filename)):

			if($nu != $n):
				$html .= '<div class="th">';
			else:
				$html .= '<div class="th active">';
			endif;

			if( true === $this->config['gallery']['rewrite']):
				$html .= 
				'<a href="' . $this->config['lgfolder'][$gallery]
				. '/' . $gallery
				. '/' . $p
				. '/' . $nu
				. '/' . $linktotype . '">'
				. '<img src="'
				. $this->config['folders'][$gallery]
				. '/' . $size
				. '/' . $filename . '"'
				. ' title=""'
				. ' alt="" />'
				. '<div>' . $text . '</div></a>'
				. '</div>' . "\n";
			else:
				$html .=
				'<a href="?g=' . $gallery
				. '&amp;p=' . $p
				. '&amp;n=' . $nu
				. '&amp;type=' . $linktotype . '">'
				. '<img src="'
				. $this->config['folders'][$gallery]
				. '/' . $size
				. '/' . $filename . '"'
				. ' title=""'
				. ' alt="" />'
				. '<div>' . $text . '</div></a>'
				. '</div>' . "\n";
			endif;
		else:
			$html .= '<div class="th end"> end of list </div>';
		endif;

		return $html;
	}


	public function picture($size,$nu,$p,$linktotype,$gallery,$text){
		$html = '';
		if(isset($this->data[$size][$nu]['filename'])):
			$filename = $this->data[$size][$nu]['filename'];
			#$image_number = $this->data[$size][$nu]['found_number'];

			switch ($this->config['gallery']['pathtype']):
    			case 'absolute':
        			$path = 'http://'
					. $this->config['gallery']['host']
					. $this->config['lgfolder'][$gallery]
					. '/'
					. $this->config['folders'][$gallery]
					. '/'
					. $size
					. '/'
					. $filename;
					$pathgetimagesize = $path;
        		break;

    			case 'absolute-docroot':
        			$path =  
					$this->config['lgfolder'][$gallery]
					. '/'
					. $this->config['folders'][$gallery]
					. '/'
					. $size
					. '/'
					. $filename;

					$pathgetimagesize = $this->config['folders'][$gallery]
					. '/'
					. $size
					. '/'
					. $filename;
        		break;

    			case 'relative':
        			$path = $this->config['folders'][$gallery]
					. '/'
					. $size
					. '/'
					. $filename;
					$pathgetimagesize = $path;
        		break;

			endswitch;

			if ($this->config['attr_height_with'][$gallery] === true){
				$s = getimagesize($pathgetimagesize);
				$w_h = $s['3'];
			}else{
				$w_h = '';
			}
			
		else:
			$filename = NULL;
		endif;

		if(is_file($this->config['folders'][$gallery] . '/' . $size . '/' . $filename)):
			
			if( true === $this->config['gallery']['rewrite']):
				$html .=
				'<a href="' . $this->config['lgfolder'][$gallery]
				. '/' . $gallery
				. '/' . $p
				. '/' . $nu
				. '/' . $linktotype . '">'
				. '<img src="'
				. $path
				. '" '
				. $w_h
				. ' title="'
				. $text
				. '" alt="" /></a>'
				. "\n";
			else:
				$html .= 
				'<a href="?g=' . $gallery
				. '&amp;p='	. $p
				. '&amp;n=' . $nu
				. '&amp;type=' . $linktotype . '">'
				. '<img src="'
				. $path
				. '" '
				. $w_h
				. ' title="'
				. $text
				. '" alt="" /></a>'
				. "\n";
			endif;
		else:
			if (DEBUG === true) $html .= "file not found";
		endif;

		return $html;
	}


	public function thumb($size,$nu,$n,$p,$linktotype,$gallery,$text){

		$html = '';
		if(isset($this->data[$size][$nu]['filename'])):
			$filename = $this->data[$size][$nu]['filename'];


			if ($this->config['gallery']['pathtype'] == 'absolute'):
				$path = 'http://'
				. $this->config['gallery']['host']
				. $this->config['lgfolder'][$gallery]
				. '/'
				. $this->config['folders'][$gallery]
				. '/'
				. $size
				. '/'
				. $filename;

				$pathgetimagesize = $path;
			endif;

			if ($this->config['gallery']['pathtype'] == 'absolute-docroot'):
				$path =  
			    $this->config['lgfolder'][$gallery]
				. '/' 
				.  $this->config['folders'][$gallery]
				. '/'
				. $size
				. '/'
				. $filename;
				# get image size need realtive or http://
				$pathgetimagesize = $this->config['folders'][$gallery]
				. '/'
				. $size
				. '/'
				. $filename;
			endif;

			if ($this->config['gallery']['pathtype'] == 'relative'):
				$path = $this->config['folders'][$gallery]
				. '/'
				. $size
				. '/'
				. $filename;
				$pathgetimagesize = $path;
			endif;



			if ($this->config['attr_height_with'][$gallery] === true){
				$s = getimagesize($pathgetimagesize);
				$w_h = $s['3'];
			}else{
				$w_h = '';
			}

		else:
			$filename = NULL;
		endif;

		if(is_file($this->config['folders'][$gallery] . '/'  . $size . '/' . $filename)):

			if ($nu == $n):
				$html .= '<li class="active">';
			else:
				$html .= '<li>';
			endif;



			if( true === $this->config['gallery']['rewrite']):
				$html .= 
				'<a href="' . $this->config['lgfolder'][$gallery]
				. '/' . $gallery
				. '/' . $p
				. '/' . $nu
				. '/'
				. $linktotype
				. '">'
				. '<img src="'
				. $path
				. '" '
				. $w_h
				. ' title="' . $text
				. '" alt="'
				. '" /></a></li>';
			else:
				$html .=
				'<a href="?g=' . $gallery
				. '&amp;p=' . $p
				. '&amp;n=' . $nu
				. '&amp;type='
				. $linktotype
				. '">'
				. '<img src="'
				. $path
				. '" '
				. $w_h
				. ' title="' . $text
				. '" alt="'
				. '" /></a></li>';
			endif;
		endif;

		return $html;
	}


	public function page_navigation($gallery,$pages,$p,$pic_per_page){

		$html = '';
		if( true === $this->config['gallery']['rewrite']):

			for($z = 0; $z < $pages; $z++):
				if($p != $z):
					$html .=
					'<li>'
					. '<a href="'
					. $this->config['lgfolder'][$gallery]
					. '/' . $gallery
					. '/' . $z
					. '/' . ($z*$pic_per_page)
					. '/sm">'
					. ($z+1)
					. '</a></li>'
					. "\n";

				else:

					$html .=
					'<li id="active"><a href="'
					. $this->config['lgfolder'][$gallery]
					. '/' . $gallery
					. '/' . $z
					. '/' . ($z*$pic_per_page)
					. '/sm">'
					. ($z+1)
					. '</a></li>'
					. "\n";
				endif;
			endfor;

		else:

			for($z = 0; $z < $pages; $z++):
				if($p != $z):
					$html .=
					'<li>'
					. '<a href="?g='
					 . $gallery
					. '&amp;p=' . $z
					. '&amp;n=' . ($z*$pic_per_page)
					. '&amp;type=sm">'
					. ($z+1)
					. '</a></li>'
					. "\n";

				else:

					$html .= 
					'<li id="active"><a href="?g='
					. $gallery
					. '&amp;p=' . $z
					. '&amp;type=sm">'
					. ($z+1)
					. '</a></li>'
					. "\n";
				endif;

			endfor;

		endif;

		return $html;
	}


}
