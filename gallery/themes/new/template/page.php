<?php
	if(!isset($gallery)):
		exit();
	endif;

	include('./gallery/config/config.gallery.php');
	include('./gallery/themes/new/config/config.page.php');

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $v->config['name'][$gallery]; ?></title>
<link href="<?php echo $config['gallery']['lgfolder']; ?>/gallery/themes/new/css/new.css" rel="stylesheet" />
</head>
<body>
<?php /**	mode tumbnail + smal pic **/
	if($_GET['type'] == 'sm'):
?>
<div id="wrapper">
<header>
<h1><?php echo $v->config['name'][$gallery]; ?></h1>
<nav id="tab">
<ul>
<?php
echo $v->page_navigation($gallery,$pages,$p,$config['template']['thumbs_per_page']);
?>
</ul>
</nav>
</header>

<div id="content">
<section id="thumbs">
<ul>
<?php

	for($nu = $start_th; $nu <= $limit_th; $nu++):
	#$size,$nu,$n,$p,$linktotype,$gallery,$text
		echo $v->thumb($config['size_th'],$nu,$n,$p,'la',$gallery,'enlage');
	endfor;
?>
</ul>
</section>
</div>

<footer>
<div>
<ul>
<li class="left"><?php echo $config['gallery']['version']; ?></li>
<li class="right">HTML 5</li>
<li class="right">Valid CSS</li>
</ul>
<section id="description">
<p><?php echo $config['template']['version']; ?></p>
</section>
<ul><li class="left">by cpb</li><li class="right"></li></ul>
<div class="clear"></div>
</div>
</footer>
</div>
<?php /** end thumbnail + smal pic **/
	endif;
?>
<?php /** mode large pic **/
	if($_GET['type'] == "la"):
?>
<section id="big_picture">
<div id="c">
<?php
	echo $v->picture($config['size_la'],$n,$p,'sm',$gallery,'back to thumbs')
?>
</div>
</section>
<?php /* end mode large pic */
	endif;
?>
</body>
</html>
