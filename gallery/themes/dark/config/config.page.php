<?php

#config.page.php

/*
* 	text of template
*/

$config['template']['version'] = 'dark v0.3';
$config['template']['thumbs_per_page'] = 9;

$pages = round( (count($data[$config['size_th']]))/($config['template']['thumbs_per_page'])+0.5,0);

$start_th = $p * $config['template']['thumbs_per_page'];
$limit_th = ($p +1) * ($config['template']['thumbs_per_page']) -1;