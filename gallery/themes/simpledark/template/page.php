<?php
	if(!isset($gallery)):
		exit();
	endif;
	include('./gallery/config/config.gallery.php');
	include('./gallery/themes/simpledark/config/config.page.php');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $v->config['name'][$gallery]; ?></title>
<link href="<?php echo $config['gallery']['lgfolder']; ?>/gallery/themes/simpledark/css/simpledark.css" rel="stylesheet">
</head>
<body>
<div id="wrapper">
<?php /*	mode tumbnail + smal pic */
	if($_GET['type'] == 'sm'):
?>
<header>
<div class="heading"><h1><?php echo $v->config['name'][$gallery]; ?></h1></div>
<nav id="tab">
<ul>
<?php
echo $v->page_navigation($gallery,$pages,$p,$pic_per_page[$config['size_th']]);
?>
</ul>
</nav>
</header>
<div id="content">
<section id="thumbs">
<ul>
<?php
	for($nu = $start_th; $nu <= $limit_th; $nu++):
	#$size,$nu,$n,$p,$linktotype,$gallery,$text
		echo $v->thumb($config['size_th'],$nu,$n,$p,'sm',$gallery,'enlage');
	endfor;
?>
</ul>
</section>
<section id="picture">
<?php
	echo $v->picture($config['size_sm'],$n,$p,'la',$gallery,'enlage');
?>
</section>
<div class="clear"></div>
</div>

<?php /* end thumbnail + smal pic */
	endif;
?>
<?php /* mode large pic */
	if($_GET['type'] == "la"):
?>
<section id="big_picture">
<?php
	echo $v->picture($config['size_la'],$n,$p,'sm',$gallery,'back to thumbs')
?>
</section>
<?php /* end mode large pic */
	endif;
?>
</div>
<footer>
<div>
<ul>
<li class="left"><?php echo $config['gallery']['version']; ?></li>
<li class="right">HTML 5</li>
<li class="right">Valid CSS</li>
</ul>
<section id="description">
<p><?php echo $config['template']['version']; ?></p>
</section>
<ul><li class="left">by cpb</li><li class="right"></li></ul>
<div class="clear"></div>
</div>
</footer>
</body>
</html>