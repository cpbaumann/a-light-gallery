/* Author:

*/
$().ready(function(){

var imagepp = 7;

if(readCookie('lgoffset') != null){
	var offset = parseInt(readCookie('lgoffset'));
//alert(offset);
}else{
	var offset = 0;
}

/*var offset = 0;*/
var thumbs = $('#thumbs li');
var numberofthumbs = thumbs.length;

$('#thumbs li').hide();
//alert(imagepp+offset)
for(var i = offset; i < imagepp+offset; i++) {
	$("#thumbs li:nth-of-type("+i+")").show();
}

$('#thumbs')
.prepend('<div class="control" id="left-btn"></div>')
.append('<div class="control" id="right-btn"></div>');

manageControls(offset);

$('.control').bind('click', function(){

	if ($(this).attr('id') == 'right-btn'){
		offset = offset + imagepp
	}
	if ($(this).attr('id') == 'left-btn'){
		offset = offset - imagepp
	}
	createCookie("lgoffset",offset,1);

	$('#thumbs li').hide();

	for(var i=0+offset; i < imagepp+offset; i++) {
		$("#thumbs li:nth-of-type("+i+")").show();
	}
	manageControls(offset);

});

function manageControls(offset){
	if(offset == 0){
		$('#left-btn').hide()
	}else{
		$('#left-btn').show()
	}
	if(offset > numberofthumbs - imagepp){
		$('#right-btn').hide()
	}else{
		$('#right-btn').show()
	}
}

});

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
createCookie(name,"",-1);
}