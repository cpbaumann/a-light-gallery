# a light gallery

A study on a Performance optimized Gallery 


## Usage

edit in `gallery/config/config.gallery.php` 

```php
$config['lgfolder'][1] = $config['gallery']['lgfolder'];
$config['folders'][1] = 'the-hotel-simple';
$config['name'][1] = 'The Hotel by J. H.';
$config['theme'][1] = 'new';
```

the folder with the images is `the-hotel-simple` is beside `/gallery/`

the uri will be `/a-light-gallery/?g=1` or in rewrite configuration `/a-light-gallery/1/0/0/sm`

the 3 sizes are defined in the global settings 

```php
$config['size_th'] = 'thumb';
$config['size_sm'] = 'small';
$config['size_la'] = 'large';
```

as folder in `/the-hotel-simple/` 

* /the-hotel-simple/thumb/ 
* /the-hotel-simple/small/ 
* /the-hotel-simple/large/ 

Other global settings are:

```php
# an version name 
$config['gallery']['version'] = 'a light gallery';
# the base folder
$config['gallery']['lgfolder'] = 'a-light-gallery';
# use rewrite true or false
$config['gallery']['rewrite'] = true;
# use absolute, absolute-docroot, relative path 
$config['gallery']['pathtype'] = 'absolute-docroot';
```

## Notes

* Requires PHP 5.2+
* optional textfile DB 

## License

This script is dual licensed under the MIT and GPL licenses.



