<?php

	/*
	*	lightgallery version 
	*/

	ob_start();
	header('Content-Type: text/html; charset=UTF-8');

	function __autoload($class_name) {
		require_once 'gallery/class/' . $class_name . '.php';
	}

	/*
	*	config of the galleries
	*/
	include('gallery/config/config.gallery.php');
	
	new ControllerLightGallery($config);	